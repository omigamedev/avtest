#include <android_native_app_glue.h>
#include <android/native_activity.h>
#include <android/log.h>
#include <android/looper.h>
#include <android/configuration.h>
#include <android/sensor.h>

#include <libavcodec/avcodec.h>

#define INBUF_SIZE 4096
#define AUDIO_INBUF_SIZE 20480
#define AUDIO_REFILL_THRESH 4096

#define LOG(fmt,...) __android_log_print(ANDROID_LOG_INFO, "native",\
    "%s:%d "fmt, __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__)
#define ERR(fmt,...) __android_log_print(ANDROID_LOG_ERROR, "native",\
    "%s:%d "fmt, __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__)

void android_main(struct android_app* app)
{
	void app_dummy();
	LOG("hello native");
	
	AVPacket avpkt;
	av_init_packet(&avpkt);
	AVCodec *codec = avcodec_find_decoder(AV_CODEC_ID_MPEG1VIDEO);
}
